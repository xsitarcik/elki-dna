package de.lmu.ifi.dbs.elki.datasource.filter.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.data.FeatureVector;
import de.lmu.ifi.dbs.elki.data.FloatVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.MultipleTypeVectorList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalMultipleID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FFTbase;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class DNA_FFT_MRA_Filter extends AbstractDNAFilter {
  private int windowSizeLo;

  private int windowSizeHi;

  private int windowShift;

  protected DNA_FFT_MRA_Filter(DNA_DataProvider provider, int windowSizeLo, int windowSizeHi, int windowShift) {
    super(provider);
    this.windowSizeLo = 1 << (int) (Math.log(windowSizeLo - 1) / Math.log(2) + 1);
    this.windowSizeHi = 1 << (int) (Math.log(windowSizeHi - 1) / Math.log(2) + 1);
    this.windowShift = windowShift;
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);
    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));

    if(currentItem instanceof StringVector_externalMultipleID) {
      data[inx] = processTask((StringVector_externalMultipleID) currentItem);
    }
    else {
      data[inx] = processTask(currentItem);
    }
  }

  protected FloatVector processTask(StringVector_externalID currentItem) {

    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, false, currentItem.getParams());
        
    float[] result = new float[windowSizeHi];

    if(dataString != null) {      
      
      float padFactor = 1;
      
      if(dataString.length<windowSizeHi){
        int origLength = dataString.length;
        dataString = Arrays.copyOfRange(dataString, 0, windowSizeHi);
        Arrays.fill(dataString, origLength, dataString.length, (char)255);
        
        padFactor = (float)dataString.length / (float)origLength;
      }
      
      // get FFT by windows
      int nWindows = 0;
      for(int i = 0; i <= dataString.length - windowSizeHi; i += windowShift) {
        int prevMax = 0;
        for(int s = windowSizeLo; s <= windowSizeHi; s *= 2) {
          for(int j = 0; j <= windowSizeHi - s; j += s) {
            char[] actualIndicators = Arrays.copyOfRange(dataString,  i+j, i+j+s);//String.copyValueOf(dataString, i + j, s).toCharArray();
            float[] actualRes = getSpectra(actualIndicators);
            actualRes[0] = 0; // eleminate DC
            for(int x = 0; x < s - prevMax; x++) {
              result[windowSizeHi - s + x] += actualRes[x];
            }
          }
          prevMax = s;
        }
        nWindows++;
      }

      // normalize result
      for(int i = 0; i < result.length; i++) {
        result[i] = result[i] / nWindows * padFactor;
      }
    }
    else {
      LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams()));
    }
    return new FloatVector(result);
  }

  @SuppressWarnings("rawtypes")
  protected MultipleTypeVectorList processTask(StringVector_externalMultipleID currentItem) {

    int fNum = currentItem.getNumberOfFiles();
    List<FeatureVector> result = new ArrayList<>();

    for(int f = 0; f < fNum; f++) {

      String fname = currentItem.getFileName(f);
      char[] dataString = provider.getData(fname, false, currentItem.getParams(f));
           
      float[] subResult = new float[windowSizeHi];

      if(dataString != null) {
        
        float padFactor = 1;
        if(dataString.length<windowSizeHi){
          int origLength = dataString.length;
          dataString = Arrays.copyOfRange(dataString, 0, windowSizeHi);
          Arrays.fill(dataString, origLength, dataString.length, (char)255);
          
          padFactor = (float)dataString.length / (float)origLength;
        }   
        
        // get FFT by windows
        int nWindows = 0;
        for(int i = 0; i <= dataString.length - windowSizeHi; i += windowShift) {
          int prevMax = 0;
          for(int s = windowSizeLo; s <= windowSizeHi; s *= 2) {
            for(int j = 0; j <= windowSizeHi - s; j += s) {
              char[] actualIndicators = Arrays.copyOfRange(dataString,  i+j, i+j+s);
              float[] actualRes = getSpectra(actualIndicators);
              actualRes[0] = 0; // eleminate DC
              for(int x = 0; x < s - prevMax; x++) {
                subResult[windowSizeHi - s + x] += actualRes[x];                
              }
            }
            prevMax = s;
          }
          nWindows++;
        }
        
        // normalize result
        for(int i = 0; i < subResult.length; i++) {
           subResult[i] = subResult[i] / nWindows * padFactor;  
        }
        
      }
      else {
        LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams(f)));
      }
      result.add(new FloatVector(subResult));
    }
    return new MultipleTypeVectorList(result);
  }
    
  private float[] getSpectra(char[] data) {

    int length = data.length;

    float re[] = new float[length];
    float[] im = new float[length];
    for(int i = 0; i < length; i++) {
      char curB = data[i];
      if(curB == 0) {
        re[i] = 1;
      }
      else if(curB == 1) {
        im[i] = -1;
      }
      else if(curB == 2) {
        im[i] = 1;
      }
      else if(curB == 3) {
        re[i] = -1;
      }
    }
    FFTbase fft = new FFTbase(length, false);
    fft.fft(re, im);

    for(int i = 0; i < length; i++) {
      re[i] = (float) (re[i] * re[i] + im[i] * im[i]);
    }

    return re;
  }

//  @Override
//  public MultipleObjectsBundle cleanUpAndReturn() {
//    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
//    res.appendColumn(TypeUtil.SPARSE_VECTOR_FIELD, Arrays.asList(data));
//    return res;
//  }
  
  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    
    if(data.getClass().getComponentType().equals(FloatVector.class)){
      res.appendColumn(TypeUtil.FLOAT_VECTOR_FIELD, Arrays.asList(data));
    }else{
      res.appendColumn(TypeUtil.MULTIPLE_TYPE_VECTOR, Arrays.asList(data));
    }
    return res;
  }

  @Override
  protected void createArray_hook() {
    data = new FloatVector[getNrOfTasks()];
  }

  public static class Parametrizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID WINW_SIZE_LO = new OptionID("filter.windowSizeLo", "Size of sliding window, min MRA, power of 2");

    protected static final OptionID WINW_SIZE_HI = new OptionID("filter.windowSizeHi", "Size of sliding window, max MRA, power of 2");

    protected static final OptionID WINW_SHIFT = new OptionID("filter.windowShift", "Window shift in 1 step");

    private int windowSizeLo;

    private int windowSizeHi;

    private int windowShift;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      IntParameter winSLP = new IntParameter(WINW_SIZE_LO);
      winSLP.addConstraint(new GreaterConstraint(0));
      IntParameter winSHP = new IntParameter(WINW_SIZE_HI);
      winSHP.addConstraint(new GreaterConstraint(0));

      IntParameter winSH = new IntParameter(WINW_SHIFT);
      winSH.addConstraint(new GreaterConstraint(0));

      if(config.grab(winSLP)) {
        windowSizeLo = winSLP.getValue();
      }

      if(config.grab(winSHP)) {
        windowSizeHi = winSHP.getValue();
      }

      if(config.grab(winSH)) {
        windowShift = winSH.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new DNA_FFT_MRA_Filter(provider, windowSizeLo, windowSizeHi, windowShift);
    }

  }

}
