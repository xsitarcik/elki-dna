package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;

import java.io.File;

import de.lmu.ifi.dbs.elki.data.stuba.StringVector;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.distance.distancefunction.AbstractPrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.result.stuba.MatrixReader;

/**
 * Ugly prototype, not for release''
 * 
 * @author Tomas
 *
 */
public class TreeClusterComparator extends AbstractPrimitiveDistanceFunction<StringVector> {

  MatrixReader mr = new MatrixReader();
  
  @Override
  public SimpleTypeInformation<? super StringVector> getInputTypeRestriction() {
    return (SimpleTypeInformation<StringVector>) TypeUtil.STRING_VECTOR;
  }

  //using only ist entry from each to compare entities exhaustive pairwise
  @Override
  public double distance(StringVector o1, StringVector o2) {

    System.out.print(o1.getValue(0));    
    double[][] ent1 = mr.readMatrix(new File(o1.getValue(1)), Integer.parseInt(o1.getValue(2)), Integer.parseInt(o1.getValue(3))).getMatrix().getArrayRef();

    System.out.println(" "+o2.getValue(0));
    double[][] ent2 = mr.readMatrix(new File(o2.getValue(1)), Integer.parseInt(o2.getValue(2)), Integer.parseInt(o2.getValue(3))).getMatrix().getArrayRef();

    double res = 0;
       
    for(int i = 0; i < ent1.length; i++) {
      for(int j = 0; j < ent1[0].length; j++) {
        res += Math.abs(ent1[i][j] - ent2[i][j]);
      }
    }
    
    return res;    
  }

}
