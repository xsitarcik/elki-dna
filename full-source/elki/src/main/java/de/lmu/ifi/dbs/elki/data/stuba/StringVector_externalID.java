package de.lmu.ifi.dbs.elki.data.stuba;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.utilities.datastructures.arraylike.ArrayAdapter;
import de.lmu.ifi.dbs.elki.utilities.io.ByteBufferSerializer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;

/**
 * String Vector containing external data source description
 * - label
 * - filename or path (to be resoved by parser)
 * - parameters (to be resoved by parser)
 * @author Tomas Farkas
 *
 */
public class StringVector_externalID extends StringVector_general implements StringVector {

  private static final Logging LOG = Logging.getLogger(StringVector_externalID.class);  
  public static final StringVector_externalID.Factory STATIC = new StringVector_externalID.Factory();
  public static final ByteBufferSerializer<StringVector_externalID> VARIABLE_SERIALIZER = new StringSerializer();

  protected StringVector_externalID(String[] values, boolean nocopy) {
    super(values, nocopy);
  }

  public StringVector_externalID(String[] values) {
    super(values);
    if(values.length<2){
      LOG.error("StringVector_externalID not containing mandatory <label, filename>");
    }
  }

  public String getLabel() {
    return this.getValue(0);
  }

  public String getFileName() {
    return this.getValue(1);
  }

  public String[] getParams() {
    if(this.values.length > 2) {
      return new String[]{this.values[2]};
    }
    return new String[0];
  }

  /**
   * 
   * @author Tomas Farkas
   *
   */
  public static class Parameterizer extends AbstractParameterizer {
    @Override
    protected StringVector_externalID.Factory makeInstance() {
      return STATIC;
    }
  }

  /**
   * 
   * @author Tomas Farkas
   *
   */
  public static class Factory implements StringVector.Factory<StringVector_externalID> {

    @Override
    public ByteBufferSerializer<StringVector_externalID> getDefaultSerializer() {
      return new StringSerializer();// VARIABLE_SERIALIZER;
    }

    @Override
    public Class<? super StringVector_externalID> getRestrictionClass() {
      return StringVector_externalID.class;
    }

    @Override
    public <A> StringVector_externalID newFeatureVector(A array, ArrayAdapter<? extends String, A> adapter) {
      int dim = adapter.size(array);
      String[] values = new String[dim];
      for(int i = 0; i < dim; i++) {
        values[i] = adapter.get(array, i);
      }
      return new StringVector_externalID(values, true);
    }

    @Override
    public StringVector_externalID newStringVector(String[] values) {
      return new StringVector_externalID(values);
    }

    @Override
    public StringVector_externalID newStringVector(List<String> values) {
      String[] array = values.toArray(new String[0]);
      return new StringVector_externalID(array);
    }

    @Override
    public StringVector_externalID newStringVector(StringVector values) {
      return new StringVector_externalID(values.toArray());
    }
  }

  /**
   * @author Tomas Farkas
   */
  public static class StringSerializer implements ByteBufferSerializer<StringVector_externalID> {

    @Override
    public StringVector_externalID fromByteBuffer(ByteBuffer buffer) throws IOException {
      String data = buffer.toString();
      String[] values = data.split(">delim<");
      return new StringVector_externalID(values, true);
    }

    @Override
    public void toByteBuffer(ByteBuffer buffer, StringVector_externalID vec) throws IOException {
      for(String s : vec.values) {
        buffer.put(s.getBytes());
        buffer.put(">delim<".getBytes());
      }
    }

    @Override
    public int getByteSize(StringVector_externalID vec) {
      int count = 0;
      for(String s : vec.values) {
        count += s.length() + 7;
      }
      return count;
    }
  }
}
