package de.lmu.ifi.dbs.elki.datasource.filter.stuba.deprecated;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;

import de.lmu.ifi.dbs.elki.data.FloatVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter.Parameterizer;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FWHT;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.LessEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.DoubleParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/**
 * DEPRECATED - use ST_Filter instead
 * @author Tomas Farkas
 *
 */
public class DNA_WHT_WindowFilter extends AbstractDNAFilter {

  private int windowSize;

  private int windowShift;

  protected DNA_WHT_WindowFilter(DNA_DataProvider provider, double sigRatio, int windowSize, int windowShift) {
    super(provider);
    this.windowSize = windowSize;
    this.windowShift = windowShift;
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);

    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));
    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, false, currentItem.getParams());

    float[] result = new float[windowSize];
    // get FWHT by windows
    for(int i = 0; i < dataString.length - windowSize; i += windowShift) {
      float[] actualIndicators = new float[windowSize];
      for(int j = 0; j < windowSize; j++) {
        if(dataString[i + j] == 0) { // using only A=1, C=-1
          actualIndicators[j] = 1;
        }
        else if(dataString[i + j] == 1)
          actualIndicators[j] = -1;
      }
      float[] actualRes = FWHT.fht(actualIndicators);
      for(int j = 0; j < windowSize; j++) {
        result[j] += Math.abs(actualRes[j]);
      }
    }
    // normalize result
    for(int i = 0; i < result.length; i++) {
      result[i] = result[i] / dataString.length;
    }
    // return
    data[inx] = new FloatVector(result);

  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    res.appendColumn(TypeUtil.SPARSE_VECTOR_FIELD, Arrays.asList(data));
    return res;
  }

  @Override
  protected void createArray_hook() {
    data = new FloatVector[getNrOfTasks()];
  }

  public static class Parametrizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID SIG_RATIO = new OptionID("filter.significantRation", "Ratio for significat values, e.g. 0.1 means select top 10%");

    protected static final OptionID WINW_SIZE = new OptionID("filter.windowSize", "Size of sliding window, power of 2");

    protected static final OptionID WINW_SHIFT = new OptionID("filter.windowShift", "Window shift in 1 step");

    private double sigRatio;

    private int windowSize;

    private int windowShift;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      DoubleParameter sigP = new DoubleParameter(SIG_RATIO);
      sigP.addConstraint(new GreaterConstraint(0));
      sigP.addConstraint(new LessEqualConstraint(1));

      IntParameter winSP = new IntParameter(WINW_SIZE);
      winSP.addConstraint(new GreaterConstraint(0));

      IntParameter winSH = new IntParameter(WINW_SHIFT);
      winSH.addConstraint(new GreaterConstraint(0));

      if(config.grab(sigP)) {
        sigRatio = sigP.getValue();
      }
      if(config.grab(winSP)) {
        windowSize = winSP.getValue();
      }

      if(config.grab(winSH)) {
        windowShift = winSH.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new DNA_WHT_WindowFilter(provider, sigRatio, windowSize, windowShift);
    }

  }

}
