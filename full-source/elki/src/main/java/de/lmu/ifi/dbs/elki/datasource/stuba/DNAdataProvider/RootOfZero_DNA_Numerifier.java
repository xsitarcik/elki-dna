package de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author Tomas Farkas
 *
 */
public class RootOfZero_DNA_Numerifier implements DNA_Numerifier {

  @Override
  public float[][] numerify(char[] inSeq, boolean dense) {

    int length = inSeq.length;
    if(dense) {
      length *= 4;
    }
    float[] re = new float[length];
    float[] im = new float[length];

    if(!dense) {

      for(int i = 0; i < inSeq.length; i++) {
        char curB = inSeq[i];
        if(curB == 0) {
          re[i] = 1;
        }
        else if(curB == 1) {
          im[i] = -1;
        }
        else if(curB == 2) {
          im[i] = 1;
        }
        else if(curB == 3) {
          re[i] = -1;
        }
      }

    }
    else {

      for(int i = 0; i < inSeq.length; i++) {
        char curB = inSeq[i];
        for(int j = 0; j < 4; j++) {

          char curb = (char) (curB & 3);
          curB = (char) (curB >> 2);

          if(curb == 0) {
            re[i * 4 + j] = 1;
          }
          else if(curb == 1) {
            im[i * 4 + j] = -1;
          }
          else if(curb == 2) {
            im[i * 4 + j] = 1;
          }
          else if(curb == 3) {
            re[i * 4 + j] = -1;
          }

        }
      }

    }
    return new float[][] { re, im };

  }

}
