package de.lmu.ifi.dbs.elki.result.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.lmu.ifi.dbs.elki.math.linearalgebra.Matrix;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class MatrixReader {
  
  public MatrixResult readMatrix(File f) {

    BufferedReader br;
    List<String> labelsC = new ArrayList<>();
    List<String> labelsR = new ArrayList<>();

    List<double[]> distances = new ArrayList<double[]>();
    int dim;

    try {
      br = new BufferedReader(new FileReader(f));

      String fstLine = br.readLine();
      String[] captions = fstLine.split("\\|");
      dim = captions.length-1;

      for(int j = 1; j < dim+1; j++) {
        labelsC.add(captions[j]);
      }

      for(;;) {
        String line = br.readLine();
        if(line == null) {
          break;
        }
        String[] lineData = line.split("\\|");
        double[] lDistcs = new double[dim];
        
        labelsR.add(lineData[0].trim());
        for(int j = 0; j < dim; j++) {
          NumberFormat format = NumberFormat.getInstance(Locale.GERMANY);
          double num = format.parse(lineData[j+1].trim()).doubleValue();
          lDistcs[j] = num;
        }
        distances.add(lDistcs);

      }
      br.close();
    }
    catch(IOException | ParseException  e) {
      e.printStackTrace();
      de.lmu.ifi.dbs.elki.logging.LoggingUtil.exception(e);
    }
    return new MatrixResult(new Matrix(distances.toArray(new double[0][])), labelsR.toArray(new String[0]), labelsC.toArray(new String[0]));
  }
  
  public MatrixResult readMatrix(File f, int startLineInx, int lineCount) {

    BufferedReader br;
    List<String> labelsC = new ArrayList<>();
    List<String> labelsR = new ArrayList<>();

    List<double[]> distances = new ArrayList<double[]>();
    int dim;

    try {
      br = new BufferedReader(new FileReader(f));

      for(int i=0;i<startLineInx;i++){
        br.readLine();
      }
      
      String fstLine = br.readLine();
      String[] captions = fstLine.split("\\|");
      dim = captions.length-1;

      for(int j = 1; j < dim+1; j++) {
        labelsC.add(captions[j]);
      }

      for(int i=0;i<lineCount-1;i++) {
        String line = br.readLine();
        String[] lineData = line.split("\\|");
        double[] lDistcs = new double[dim];
        
        labelsR.add(lineData[0].trim());
        for(int j = 0; j < dim; j++) {
          NumberFormat format = NumberFormat.getInstance(Locale.GERMANY);
          double num = format.parse(lineData[j+1].trim()).doubleValue();
          lDistcs[j] = num;
        }
        distances.add(lDistcs);

      }
      br.close();
    }
    catch(IOException | ParseException  e) {
      e.printStackTrace();
      de.lmu.ifi.dbs.elki.logging.LoggingUtil.exception(e);
    }
    return new MatrixResult(new Matrix(distances.toArray(new double[0][])), labelsR.toArray(new String[0]), labelsC.toArray(new String[0]));
  }
  
}
