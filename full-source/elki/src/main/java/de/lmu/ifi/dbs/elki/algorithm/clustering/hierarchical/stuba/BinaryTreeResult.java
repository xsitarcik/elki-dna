package de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.PointerHierarchyRepresentationResult;
import de.lmu.ifi.dbs.elki.database.datastore.DBIDDataStore;
import de.lmu.ifi.dbs.elki.database.datastore.DataStoreFactory;
import de.lmu.ifi.dbs.elki.database.datastore.DataStoreUtil;
import de.lmu.ifi.dbs.elki.database.datastore.DoubleDataStore;
import de.lmu.ifi.dbs.elki.database.datastore.IntegerDataStore;
import de.lmu.ifi.dbs.elki.database.datastore.WritableDBIDDataStore;
import de.lmu.ifi.dbs.elki.database.datastore.WritableDoubleDataStore;
import de.lmu.ifi.dbs.elki.database.ids.DBID;
import de.lmu.ifi.dbs.elki.database.ids.DBIDIter;
import de.lmu.ifi.dbs.elki.database.ids.DBIDRef;
import de.lmu.ifi.dbs.elki.database.ids.DBIDUtil;
import de.lmu.ifi.dbs.elki.database.ids.DBIDs;
import de.lmu.ifi.dbs.elki.math.linearalgebra.Matrix;
import de.lmu.ifi.dbs.elki.result.Result;
import de.lmu.ifi.dbs.elki.utilities.pairs.Pair;

/**
 * 
 * @author Tomas Farkas designed for rtg-elki integration
 */

public class BinaryTreeResult implements Result {

  BinaryTreeResult left;

  BinaryTreeResult right;

  double height = 0;

  double ldist;

  double rdist;

  DBIDRef label;

  BinaryTreeResult parent;

  public BinaryTreeResult(BinaryTreeResult left, BinaryTreeResult right, double ldist, double rdist, DBIDRef label) {
    this.left = left;
    this.right = right;
    this.ldist = ldist;
    this.rdist = rdist;
    this.label = label;
  }

  public BinaryTreeResult(BinaryTreeResult left, BinaryTreeResult right, double ldist, double rdist, double height, DBIDRef label) {
    this.left = left;
    this.right = right;
    this.ldist = ldist;
    this.rdist = rdist;
    this.height = height;
    this.label = label;
  }

  @SuppressWarnings("deprecation")
  public static BinaryTreeResult fromPHRR(PointerHierarchyRepresentationResult data) {

    // ArrayDBIDs IDsOrder =
    // PointerHierarchyRepresentationResult.topologicalSort(data.getDBIDs(),
    // data.getParentStore(), data.getParentDistanceStore());
    DBIDs IDs = data.getDBIDs();
    IntegerDataStore IDsOrder = data.getPositions();
    BinaryTreeResult[] nodes = new BinaryTreeResult[IDs.size()];

    DBIDDataStore parents = data.getParentStore();
    DoubleDataStore dists = data.getParentDistanceStore();

    // init leaf nodes
    for(DBIDIter iter = IDs.iter(); iter.valid(); iter.advance()) {
      DBID id = DBIDUtil.deref(iter);
      System.out.println(IDsOrder.get(id));
      nodes[IDsOrder.get(id)] = new BinaryTreeResult(null, null, 0.0, 0.0, id);
    }
    
   
    // while not converges
    boolean change = true;
    int changeInx = 0;
    
    while(change) {
      change = false;         
      
      // connect neighbour nodes
      for(int i = 0; i < nodes.length - 1; i++) {
        if(nodes[i] == null) {
          continue;
        }
        DBIDRef thisN = nodes[i].label;
        DBID parent = parents.get(nodes[i].label);
        DBIDRef next = null;
        int j;
        for(j = i + 1; j < nodes.length; j++) {
          if(nodes[j] != null) {
            next = nodes[j].label;
            break;
          }
        }
        if(next != null && parent.internalGetIndex() == next.internalGetIndex() && thisN.internalGetIndex() != next.internalGetIndex()) {

          double height = dists.get(nodes[i].label);

          BinaryTreeResult newNode = new BinaryTreeResult(nodes[i], nodes[j], height - nodes[i].height, height - nodes[j].height, height, next);
          nodes[j] = null;
          nodes[i] = newNode;

          changeInx = i;
          change = true;
          break;          
        }
      }
    }
    BinaryTreeResult result = nodes[changeInx];
   
    // clear labels in non-leaf nodes
    result.clearNonLeafLabels();

    // return
    return result;

  }

  public PointerHierarchyRepresentationResult getPHRR(DBIDs ids) {

    WritableDBIDDataStore parents = DataStoreUtil.makeDBIDStorage(ids, DataStoreFactory.HINT_STATIC);
    WritableDoubleDataStore distances = DataStoreUtil.makeDoubleStorage(ids, DataStoreFactory.HINT_STATIC, Double.POSITIVE_INFINITY);

    // update all tree parents and heights
    this.parentifyAll();
    this.getHeight();

    // find ids in tree
    Map<Integer, BinaryTreeResult> indices = getIndices(new HashMap<>());

    // foreach node: find its rightmost sibling and distance to the top
    for(DBIDIter iter = ids.iter(); iter.valid(); iter.advance()) {
      DBIDRef nodeRef = iter;
      BinaryTreeResult tree = indices.get(nodeRef.internalGetIndex());
      double distance = Double.POSITIVE_INFINITY;

      // traverse up to such a parent that has a right child that is not this
      BinaryTreeResult current = tree;
      boolean isNoParent = false;

      while(true) {
        if(current.parent == null) {
          // this is the no-parent leaf
          isNoParent = true;
          break;
        }
        else {
          if(current.parent.right != null && current.parent.right == current) {
            // coming from right -> go up
            current = current.parent;
          }
          else if(current.parent.left != null && current.parent.left == current) {
            // coming from left
            if(current.parent.right != null) {
              // if theres right child -> stop
              current = current.parent;
              break;
            }
            else {
              // else -> go up
              current = current.parent;
            }
          }
        }
      }

      distance = current.height;

      while(current.right != null) {
        current = current.right;

      }

      if(!isNoParent) {
        parents.put(nodeRef, current.label);
        distances.put(nodeRef, distance);
      }
      else {
        parents.put(nodeRef, nodeRef);
        distances.put(nodeRef, Double.POSITIVE_INFINITY);
      }

    }

    return new PointerHierarchyRepresentationResult(ids, parents, distances);
  }

  public Pair<List<Integer>, Matrix> getElementsDistanceMatrix(boolean useDistances) {

    this.parentifyAll();

    int nrNodes = this.getNrOfNodes(false);

    double[][] W = new double[nrNodes][nrNodes];
    for(int i = 0; i < nrNodes; i++) {
      for(int j = 0; j < nrNodes; j++) {
        W[i][j] = Double.POSITIVE_INFINITY;
      }
      W[i][i] = 0;
    }

    // initial fill
    List<BinaryTreeResult> allNodes = this.getAllNodesInorder(new ArrayList<>());
    for(int i = 0; i < allNodes.size(); i++) {
      BinaryTreeResult curNode = allNodes.get(i);
      if(curNode.left != null) {
        int lInx = allNodes.indexOf(curNode.left);
        W[i][lInx] = W[lInx][i] = useDistances ? curNode.ldist : 1;
      }
      if(curNode.right != null) {
        int rInx = allNodes.indexOf(curNode.right);
        W[i][rInx] = W[rInx][i] = useDistances ? curNode.rdist : 1;
      }
    }

    // FLOYD-WARSHALL
    for(int k = 0; k < nrNodes; k++) {
      for(int i = 0; i < nrNodes; i++) {
        for(int j = 0; j < nrNodes; j++) {
          W[i][j] = Math.min(W[i][j], W[i][k] + W[k][j]);
        }
      }
    }

    // sort and return
    Matrix result = new Matrix(W);

    Map<Integer, Integer> leafNodeIndicesMap = new TreeMap<>();

    for(int i = 0; i < nrNodes; i++) {
      if(allNodes.get(i).label != null) {
        leafNodeIndicesMap.put(allNodes.get(i).label.internalGetIndex(), i);
      }
    }

    List<Integer> leafNodeIndices = new ArrayList<>();
    SortedSet<Integer> keys = new TreeSet<Integer>(leafNodeIndicesMap.keySet());
    for(Integer key : keys) {
      leafNodeIndices.add(leafNodeIndicesMap.get(key));
    }

    int[] a = leafNodeIndices.stream().mapToInt(Integer::intValue).toArray();
    return new Pair<>(new ArrayList<>(keys), result.getMatrix(a, a));
  }

  /////////////////////////////////////////////////////////////////////////////////////////

  private int getNrOfNodes(boolean leafOnly) {

    int result = 1;
    if(leafOnly && this.label == null) {
      result = 0;
    }

    if(this.left != null) {
      result += left.getNrOfNodes(leafOnly);
    }
    if(this.right != null) {
      result += right.getNrOfNodes(leafOnly);
    }
    return result;
  }

  private List<BinaryTreeResult> getAllNodesInorder(List<BinaryTreeResult> result) {

    if(this.left != null) {
      left.getAllNodesInorder(result);
    }
    result.add(this);
    if(this.right != null) {
      right.getAllNodesInorder(result);
    }
    return result;
  }

  private void clearNonLeafLabels() {
    if(this.left != null) {
      this.left.clearNonLeafLabels();
      this.label = null;
    }
    if(this.right != null) {
      this.right.clearNonLeafLabels();
      this.label = null;
    }
  }

  private void parentifyAll() {
    if(left != null) {
      left.parent = this;
      left.parentifyAll();
    }
    if(right != null) {
      right.parent = this;
      right.parentifyAll();
    }
  }

  private double getHeight() {
    double lH = 0;
    double rH = 0;

    if(left != null) {
      lH = left.getHeight() + ldist;
    }
    if(right != null) {
      rH = right.getHeight() + rdist;
    }

    this.height = lH > rH ? lH : rH;

    return this.height;
  }

  public void inOrder(int dep, BinaryTreeResult root) {
    if(root != null) {
      inOrder(dep + 1, root.left);
      inOrder(dep + 1, root.right);
    }
  }

  // DBIDRef improperly implements "equals" -> hashmap does not work with
  // DBIDRef as a key
  private Map<Integer, BinaryTreeResult> getIndices(Map<Integer, BinaryTreeResult> result) {
    if(label != null) {
      result.put(label.internalGetIndex(), this);
    }
    if(right != null) {
      right.getIndices(result);
    }
    if(left != null) {
      left.getIndices(result);
    }

    return result;
  }

  @Deprecated
  public void printTree(int dep) {
    if(left != null) {
      System.out.println("left " + ldist);
      left.printTree(dep + 1);
    }
    System.out.println(dep + " " + this.label);
    if(right != null) {
      System.out.println("right " + rdist);
      right.printTree(dep + 1);
    }
  }
  
  public String toNewick(){
   
    if(right==null && left==null){
      return label.toString();
    }
    
    String output = "(";
    if(right!=null){
      output+=right.toNewick()+":"+rdist;
    }
    if(right!=null && left!=null){
      output+=",";
    }
    if(left!=null){
      output+=left.toNewick()+":"+ldist;
    }
    output += ")";    
    
    return output;
  }

  @Override
  public String getLongName() {
    return "Hierarchical Clusterng Binary Tree Result";
  }

  @Override
  public String getShortName() {
    return "HierBTreeResult";
  }

  public String getDecription() {
    return this.toString().substring(77) 
        + " left: " + (left == null ? "null" : left.toString().substring(77))
            + ", right: " + (right == null ? "null" : right.toString().substring(77)) 
                + ", height: " + height 
                + " ,ldist: " + ldist 
                + " ,rdist: " + rdist 
                + " ,label: " + label 
                + " ,parent: " + (parent==null?"null":parent.toString().substring(77));
  }

}
