package de.lmu.ifi.dbs.elki.visualization.visualizers.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.HashMap;
import java.util.Map;

import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.database.Database;
import de.lmu.ifi.dbs.elki.database.ids.DBID;
import de.lmu.ifi.dbs.elki.database.ids.DBIDIter;
import de.lmu.ifi.dbs.elki.database.ids.DBIDUtil;
import de.lmu.ifi.dbs.elki.database.relation.Relation;
import de.lmu.ifi.dbs.elki.logging.Logging;

/**
 * LabelProvider implementation that uses the first occurence of LabelList
 * relation in specified database as label list
 * 
 * @author Tomas Farkas
 */
public class RelationLabelProvider implements LabelProvider {

  private static final Logging LOG = Logging.getLogger(RelationLabelProvider.class);

  protected Map<Integer, LabelList> labelMap = new HashMap<>();

  @Override
  public String getLabel(DBID id) {
    LabelList res = labelMap.get(id.internalGetIndex());
    return res != null ? res.toString() : "N/A";
  }
  
  @Override
  public String getLabel(Integer id) {
    LabelList res = labelMap.get(id);
    return res != null ? res.toString() : "N/A";
  }


  @Override
  public void setDatabase(Database db) {
    Relation<LabelList> labels = db.getRelation(TypeUtil.LABELLIST, new Object[] {});
    Relation<DBID> ids = db.getRelation(TypeUtil.DBID, new Object[] {});

    if(labels == null) {
      LOG.warning("DB not containing relation of labels");
    }
    else{
      for(DBIDIter it = ids.iterDBIDs(); it.valid(); it.advance()) {
        DBID id = DBIDUtil.deref(it);
        LabelList lab = labels.get(it);
        
        labelMap.put(id.internalGetIndex(), lab);
      }
    }
  }

  
}
