package de.lmu.ifi.dbs.elki.math.stuba.transform;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FFTbase;

/**
 * 
 * @author Tomas
 *
 */
public class FFTTransformer implements SpectralTransformer{
  
  @Override
  public float[] getSpectrum(boolean direct, boolean clearDC, float[]... arg) {
    
    float[] re = arg[0];
    float[] im;
    
    if(arg.length < 2){
      im = new float[re.length];
    }else{
      im = arg[1];
    }    
    
    FFTbase impl = new FFTbase(re.length, !direct);

    impl.fft(re, im);
  
    float[] res = new float[re.length];
    for(int i=0;i<re.length;i++){
      res[i] = (float) Math.sqrt(re[i]*re[i] + im[i]*im[i]);
    }
    
    if(clearDC){
      res[0] = 0;
    }
    
    return res;
  }

}


