#!/bin/csh
set noglob

set wl = 2048
set ws = 256
set enc = "COMPLEMENTS_TOGETHER"

if (!($#argv)) then
	echo "***Sliding window spectral projection method***"
        echo "Usage:"
        echo "  ./SWPM.sh [Required input] [Additional input]"
        echo "Required input:"
        echo "  -i <string>     Path to input config file"
        echo "  -o <string>     Path where files will be output"
        echo "Additional input:"
        echo "  -s <int>        Step of sliding window(default 256)"
        echo "  -l <int>        Length of sliding window(default 2048)"
        echo "  -c              use for complements-cross method of DNA encoding (default complements together)"
       	exit 1
endif
while($#argv)
	switch($argv[1])
		case -i:
			shift
			set in = $argv[1]
			breaksw
		case -o:
			shift
			set out = $argv[1]
			breaksw
		case -l:
			shift
			set wl = $argv[1]
			breaksw
		case -s:
			shift
			set ws = $argv[1]
			breaksw
		case -c:
			set enc = "COMPLEMENTS_CROSS"
			breaksw
		default:
			echo "***Sliding window spectral projection method***"
			echo "Usage:"
			echo "  ./SWPM.sh [Required input] [Additional input]"
			echo "Required input:"
			echo "  -i <string>	Path to input config file"
			echo "  -o <string>	Path where files will be output"
			echo "Additional input:"
			echo "  -s <int> 	Step of sliding window(default 256)"
			echo "  -l <int>	Length of sliding window(default 2048)"
			echo "  -c 		use for complements-cross method of DNA encoding (default complements together)"
			exit 1
	endsw
	shift
end


java -cp "./elki_nr.jar:./dependency/*" de.lmu.ifi.dbs.elki.application.KDDCLIApplication \
   	-dbc.in $in \
	-dbc.parser stuba.StringVectorParser \
	-parser.colsep ,\\s \
	-parser.vector-type StringVector_externalID \
	-dbc.filter stuba.DNA_ST_WindowFilter \
	-filter.dataProvider FastaDnaDataProvider \
	-filter.numerifier RootOfZero_DNA_Numerifier \
	-numerifier.encoding $enc \
	-filter.transform FFTTransformer \
	-filter.windowSize $wl \
	-filter.windowShift $ws \
	-algorithm clustering.hierarchical.SLINK,stuba.DistanceMatrixCreator \
	-algorithm.distancefunction minkowski.EuclideanDistanceFunction \
	-resulthandler stuba.ExportLabelledVisualizations,stuba.MatrixWriter,stuba.NewickTreeResultWriter \
	-vis.output $out \
	-rw.output $out/distmx.txt \
	-rw.output $out/newick.txt >& /dev/stdout
	
