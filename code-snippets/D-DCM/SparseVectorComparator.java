package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.data.SparseNumberVector;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.distance.distancefunction.AbstractPrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.EnumParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/**
 * Compare two sparse vectors in n + m
 * 
 * @author Tomas Farkas, inspired by Shoshan Cohen
 *
 */

public class SparseVectorComparator extends AbstractPrimitiveDistanceFunction<SparseNumberVector> {

  protected SparseCompareType compType;

  protected int tolerance;

  public SparseVectorComparator(SparseCompareType compType, int tolerance) {
    this.compType = compType;
    this.tolerance = tolerance;
  }

  @Override
  public SimpleTypeInformation<? super SparseNumberVector> getInputTypeRestriction() {
    return (SimpleTypeInformation<? super SparseNumberVector>) TypeUtil.SPARSE_VECTOR_FIELD;
  }

  @Override
  public double distance(SparseNumberVector o1, SparseNumberVector o2) {

    // no hook methods nor OO principles for sake of performance
    if(tolerance == 0) { // default n+m behaviour
      switch(this.compType){
      case SUBTRACTIVE__NONMATCH_IGNORANT:
        return subtractiveDist(o1, o2);
      case DIVISIVE__NONMATCH_IGNORANT:
        return divisiveDist(o1, o2);
      case NONMATCH_SENSITIVE:
        return nonMatchDist(o1, o2);
      default:
        return 0;
      }
    }
    return 0;
  }

  private double subtractiveDist(SparseNumberVector o1, SparseNumberVector o2) {
    int o1Iter = o1.iter();
    int o2Iter = o2.iter();
    double result = 0;

    while(o1.iterValid(o1Iter) && o2.iterValid(o2Iter)) {
      if(o1.iterDim(o1Iter) < o2.iterDim(o2Iter)) {
        o1Iter = o1.iterAdvance(o1Iter);
      }
      else if(o1.iterDim(o1Iter) > o2.iterDim(o2Iter)) {
        o2Iter = o2.iterAdvance(o2Iter);
      }
      else {
        double v1 = o1.iterDoubleValue(o1Iter);
        double v2 = o2.iterDoubleValue(o2Iter);
        if(0 == v1 && 0 == v2) {
          continue;
        }
        result += Math.abs(v1 - v2);
        o1Iter = o1.iterAdvance(o1Iter);
        o2Iter = o2.iterAdvance(o2Iter);
      }
    }
    return result;
  }

  private double divisiveDist(SparseNumberVector o1, SparseNumberVector o2) {
    int o1Iter = o1.iter();
    int o2Iter = o2.iter();
    double result = 0;

    while(o1.iterValid(o1Iter) && o2.iterValid(o2Iter)) {
      if(o1.iterDim(o1Iter) < o2.iterDim(o2Iter)) {
        o1Iter = o1.iterAdvance(o1Iter);
      }
      else if(o1.iterDim(o1Iter) > o2.iterDim(o2Iter)) {
        o2Iter = o2.iterAdvance(o2Iter);
      }
      else {
        double v1 = o1.iterDoubleValue(o1Iter);
        double v2 = o2.iterDoubleValue(o2Iter);
        if(0 == v1 && 0 == v2) {
          continue;
        }
        result += Math.abs(v1 - v2) / (Math.abs(v1) > Math.abs(v2) ? Math.abs(v1) : Math.abs(v2));
        o1Iter = o1.iterAdvance(o1Iter);
        o2Iter = o2.iterAdvance(o2Iter);
      }
    }
    return result;
  }

  private double nonMatchDist(SparseNumberVector o1, SparseNumberVector o2) {
    int o1Iter = o1.iter();
    int o2Iter = o2.iter();
    int matchCnt = 0;

    while(o1.iterValid(o1Iter) && o2.iterValid(o2Iter)) {
      if(o1.iterDim(o1Iter) < o2.iterDim(o2Iter)) {
        o1Iter = o1.iterAdvance(o1Iter);
      }
      else if(o1.iterDim(o1Iter) > o2.iterDim(o2Iter)) {
        o2Iter = o2.iterAdvance(o2Iter);
      }
      else {
        matchCnt++;
        o1Iter = o1.iterAdvance(o1Iter);
        o2Iter = o2.iterAdvance(o2Iter);
      }
    }
    double result = (double)matchCnt / (double)(o1.getDimensionality() > o2.getDimensionality() ? o2.getDimensionality() : o1.getDimensionality());
    return 1-result;
  }


  public enum SparseCompareType {
    SUBTRACTIVE__NONMATCH_IGNORANT, DIVISIVE__NONMATCH_IGNORANT, NONMATCH_SENSITIVE
  }

  public static class Parameterizer<O> extends AbstractParameterizer {

    protected static final OptionID COMP_TYPE = new OptionID("dist.compType", "Comparing realization");

    protected SparseCompareType compType = null;

    protected static final OptionID TOLERANCE = new OptionID("dist.tolerance", "Absolute tolerance in index");

    protected int tolerance;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      final EnumParameter<SparseCompareType> param = new EnumParameter<>(COMP_TYPE, SparseCompareType.class, SparseCompareType.NONMATCH_SENSITIVE);
      if(config.grab(param)) {
        compType = param.getValue();
      }

      final IntParameter param2 = new IntParameter(TOLERANCE, 0);
      param2.addConstraint(new GreaterConstraint(0));
      if(config.grab(param2)) {
        tolerance = param2.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new SparseVectorComparator(compType, tolerance);
    }

  }
}
