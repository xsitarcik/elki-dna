package de.lmu.ifi.dbs.elki.datasource.filter.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.algorithm.stuba.MemoryAwareTask;
import de.lmu.ifi.dbs.elki.data.DoubleVector;
import de.lmu.ifi.dbs.elki.data.FeatureVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.MultipleTypeVectorList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalMultipleID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.StringParameter;

/**
 * Spaced word frequency filter implementation
 * 
 * @author Tomas Farkas *
 * @tested 24/07/16 OK working with DNA data with 4 bases encoded in 1 byte
 */
public class DNA_SpacedWordFrequencyFilter extends AbstractDNAFilter implements MemoryAwareTask {

  protected final long pattern;

  protected DNA_SpacedWordFrequencyFilter(DNA_DataProvider provider, long pattern) {
    super(provider);
    this.pattern = pattern;
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);
    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));

    if(currentItem instanceof StringVector_externalMultipleID) {
      data[inx] = processTask((StringVector_externalMultipleID) currentItem);
    }
    else {
      data[inx] = processTask(currentItem);
    }
  }

  protected DoubleVector processTask(StringVector_externalID currentItem) {

    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, true, currentItem.getParams());
    if(dataString != null) {
      double[] frequencies = getFrequencies(dataString);
      return new DoubleVector(frequencies);
    }
    else {
      LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams()));
      return new DoubleVector(new double[0]);

    }
  }

  @SuppressWarnings("rawtypes")
  protected MultipleTypeVectorList processTask(StringVector_externalMultipleID currentItem) {

    int fNum = currentItem.getNumberOfFiles();
    List<FeatureVector> result = new ArrayList<>();

    for(int f = 0; f < fNum; f++) {
      String fname = currentItem.getFileName(f);
      char[] dataString = provider.getData(fname, true, currentItem.getParams(f));
      if(dataString != null) {
        double[] frequencies = getFrequencies(dataString);
        result.add(new DoubleVector(frequencies));
      }
      else {
        LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams(f)));
        result.add(new DoubleVector(new double[0]));
      }
    }
    return new MultipleTypeVectorList(result);
  }

  @Override
  public long getCurrentMemoryUsage() {
    return 0;
  }

  protected double[] getFrequencies(char[] dataString) {
    int cardinality = Long.bitCount(pattern);
    int vecLength = 4 << (cardinality - 2); // 4^card

    int[] counts = new int[vecLength];
    double[] res = new double[vecLength];
    long totalCount = 0;
    // process stream
    long curWord = 0;
    for(int i = 0; i < dataString.length; i++) {
      char cur4B = dataString[i];
      for(int j = 0; j < 4; j++) {
        long andRes = curWord & pattern;
        int inx = shiftDown(andRes);
        counts[inx]++;
        totalCount++;

        char curB = (char) ((cur4B & 192) >> 6);
        cur4B = (char) (cur4B << 2);

        curWord = curWord << 2;
        curWord += curB;
      }
    }
    // append last
    long andRes = curWord & pattern;
    int inx = shiftDown(andRes);
    counts[inx]++;
    totalCount++;
    // remove incomplete
    curWord = 0;
    int n = 0;
    for(int i = 0; i < dataString.length; i++) {
      char cur4B = dataString[i];
      for(int j = 0; j < 4; j++) {
        andRes = curWord & pattern;
        inx = shiftDown(andRes);
        counts[inx]--;
        totalCount--;
        n++;
        if(n == (Long.BYTES * 8 - Long.numberOfLeadingZeros(pattern)) / 2) {
          break;
        }

        char curB = (char) ((cur4B & 192) >> 6);
        cur4B = (char) (cur4B << 2);

        curWord = curWord << 2;
        curWord += curB;
      }
      if(n == (Long.BYTES * 8 - Long.numberOfLeadingZeros(pattern)) / 2) {
        break;
      }
    }

    // normalize
    for(int i = 0; i < vecLength; i++) {
      double d = counts[i] * 1000.0 / totalCount;
      res[i] = d;
    }

    return res;
  }

  private int shiftDown(long word) {
    int curInx = 0;
    long one = 1;
    for(int i = Long.BYTES * 8 - 1; i >= 0; i--) {
      if((pattern & (one << i)) > 0) {
        curInx = (curInx << 1);
        if((word & (one << i)) > 0) {
          curInx++;
        }
      }
    }
    return curInx;
  }

  @Override
  protected void createArray_hook() {
    data = new DoubleVector[getNrOfTasks()];
  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    if(data.getClass().getComponentType().equals(DoubleVector.class)){
      res.appendColumn(TypeUtil.DOUBLE_VECTOR_FIELD, Arrays.asList(data));
    }else{
      res.appendColumn(TypeUtil.MULTIPLE_TYPE_VECTOR, Arrays.asList(data));
    }
    return res;
  }

  public static class Parameterizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID PATTERN = new OptionID("filter.pattern", "Pattern to specify spaced word up to 32 chars of '0' or '1'");

    protected String pattern = "";

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      String bitPatt = "1";
      StringParameter pattP = new StringParameter(PATTERN, "1");
      if(config.grab(pattP)) {
        pattern = "";
        bitPatt = pattP.getValue();
      }
      for(int i = 0; i < bitPatt.length(); i++) {
        pattern += bitPatt.charAt(i); // duplicate each char (base encoded by 2
                                      // bits)
        pattern += bitPatt.charAt(i);
      }
    }

    @Override
    protected DNA_SpacedWordFrequencyFilter makeInstance() {
      return new DNA_SpacedWordFrequencyFilter(provider, Long.parseLong(pattern, 2));
    }
  }

}
